import json
import requests
from lxml import html

def get_urls():
  urls = []
#   for i in range(1,100):
  for i in range(1,2):
    i = str(i)
    if len(i) == 1:
      i = '0'+i
    url = 'https://kladr-rf.ru/{}/'.format(i)
    urls.append(url)
  return urls


def dump_table(page):
  res = []
  for i in page.xpath('//tbody/tr/td'):
    try:
      res.append(int(i.text))
    except:
      res.append(0)
  return {
    "code": res[0],
    "zip": res[1],
    "okato": res[2],
    "nalog": res[3],
  }


def dump_district(page):
  district = []
  for item in page.xpath('//div[@class="row-fluid"]/div[@class="span4"]/p/a'):
    district.append(
      parse_district_page(item.text, item.xpath('@href')[0])
    )
    print(item.text)
  return district



def parse_district_page(name, url):
  result = {
    "name": name,
    "url": url
  }

  answer = requests.get(url)
  text = answer.content.decode()
  page = html.fromstring(text)
  result.update(**dump_table(page))
  result["district"] = dump_district(page)
  return result


def parse_region_page(url):
  answer = requests.get(url)
  text = answer.content.decode()
  page = html.fromstring(text)

  result = {
    "name": page.xpath('//ul[@class="breadcrumb"]/li/a')[0].text,
    "url": url,
    "code_kladr": page.xpath('//em')[0].text,
  }
  result.update(**dump_table(page))
  result["district"] = dump_district(page)

  return result

result = []

for url in get_urls():
  result.append(
    parse_region_page(url)
  )


print(
  json.dumps(
    result
  )
)


